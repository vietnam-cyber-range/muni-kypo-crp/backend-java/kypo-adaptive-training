package cz.muni.ics.kypo.training.adaptive.enums;

public enum SubmissionType {

    CORRECT,
    INCORRECT
}

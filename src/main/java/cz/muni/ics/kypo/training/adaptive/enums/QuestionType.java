package cz.muni.ics.kypo.training.adaptive.enums;

/**
 * The enum Question type.
 */
public enum QuestionType {
    /**
     * Free-form question type.
     */
    FFQ,
    /**
     * Multiple choice question type.
     */
    MCQ,
    /**
     * Rate form question type.
     */
    RFQ
}

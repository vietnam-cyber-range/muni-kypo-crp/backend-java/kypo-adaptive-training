package cz.muni.ics.kypo.training.adaptive.enums;

/**
 * States represented in Training Run entity.
 */
public enum TRState {

    /**
     * Running run state.
     */
    RUNNING,
    /**
     * Finished run state.
     */
    FINISHED,
    /**
     * Archived run state.
     */
    ARCHIVED
}

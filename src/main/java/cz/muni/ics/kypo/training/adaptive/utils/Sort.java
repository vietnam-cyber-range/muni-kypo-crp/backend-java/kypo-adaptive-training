package cz.muni.ics.kypo.training.adaptive.utils;

public abstract class Sort {
    public static final String ASC = "asc";
    public static final String DESC = "desc";
}

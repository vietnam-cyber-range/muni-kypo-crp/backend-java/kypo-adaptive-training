package cz.muni.ics.kypo.training.adaptive.repository.phases;

import cz.muni.ics.kypo.training.adaptive.domain.phase.QuestionnairePhase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionnairePhaseRepository extends JpaRepository<QuestionnairePhase, Long>, QuerydslPredicateExecutor<QuestionnairePhase> {
}

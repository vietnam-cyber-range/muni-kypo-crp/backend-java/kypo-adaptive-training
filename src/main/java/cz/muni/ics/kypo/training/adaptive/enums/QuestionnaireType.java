package cz.muni.ics.kypo.training.adaptive.enums;

public enum QuestionnaireType {
    ADAPTIVE,
    GENERAL
}

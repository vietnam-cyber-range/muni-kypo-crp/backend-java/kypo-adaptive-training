package cz.muni.ics.kypo.training.adaptive.enums;

public enum PhaseType {
    QUESTIONNAIRE,
    INFO,
    TRAINING,
    ACCESS
}
